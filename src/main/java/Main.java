import executors.Executor;
import handlers.FileHandler;
import parsers.FileParser;
import procedures.Procedure;
import results.Result;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IllegalAccessException, ClassNotFoundException,
            InstantiationException, IOException, NoSuchMethodException, InvocationTargetException {
        FileParser<Result<?>> fileParser = new FileParser<>(args);
        List<Procedure<Result<?>>> procedures = fileParser.getProcedures();
        List<String> fileNames = fileParser.getFileNames();
        Executor executor = new Executor();
        FileHandler fileHandler = new FileHandler();
        for (String fileName : fileNames) {
            List<String> words = fileHandler.readWords(fileName);
            executor.execute(fileName, words, procedures);
        }

        executor.shutdownExecutor();
    }
}
