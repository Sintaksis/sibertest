package handlers;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TextHandler {
    public List<List<String>> splitForConcurrent(List<String> inputStringList, int threadCount) {
        List<List<String>> resultList = new ArrayList<>();
        int wordsNumber = inputStringList.size();
        int divisor = wordsNumber / threadCount;
        int startIndex;
        int endIndex = divisor;
        for (int i = 0; i < threadCount; i++) {
            boolean lastIteration = (i * divisor) == (divisor * threadCount - divisor);
            startIndex = i * divisor;
            if (lastIteration) {
                endIndex = wordsNumber;
            }
            resultList.add(inputStringList.subList(startIndex, endIndex));
            endIndex += divisor;
        }

        return resultList;
    }

    public List<String> splitBySpace(List<String> stringList) {
        List<String> splitedWords = new LinkedList<>();
        String word;
        int startIndex = 0;
        int endIndex;
        for (String s : stringList) {
            for (int i = 0; i < s.length(); i++) {
                if (!s.contains(" ")) {
                    splitedWords.add(s);
                    break;
                }
                endIndex = i;
                if (s.charAt(i) == ' ' || i == s.length() - 1) {
                    word = s.substring(startIndex, endIndex + 1).trim();
                    splitedWords.add(word);
                    startIndex = i + 1;
                }
            }
            startIndex = 0;
        }

        return splitedWords;
    }
}
