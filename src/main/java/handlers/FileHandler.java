package handlers;

import results.Result;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class FileHandler {
    private final TextHandler textHandler = new TextHandler();

    public FileHandler() {
    }

    public List<String> readWords(String filePath) throws IOException {
        List<String> words = new LinkedList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String temp;
            while ((temp = reader.readLine()) != null) {
                words.add(temp);
            }
        }

        return textHandler.splitBySpace(words);
    }

    public void printResult(Result<?> result, String className, String fileName) throws IOException {
        try (FileWriter writer = new FileWriter("results/" + className + "_" + fileName)) {
            String resultValue;
            if ((resultValue = result.getResultToString()) != null) {
                writer.append(resultValue);
            }
        }

        System.out.println("Execution of \"" + className + "\"" + " procedure in file \"" + fileName + "\" completed!");
    }
}
