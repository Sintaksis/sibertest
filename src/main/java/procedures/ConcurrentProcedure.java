package procedures;

import results.Result;
import results.impl.ResultOfResult;

public interface ConcurrentProcedure<T extends Result<?>> extends Procedure<T> {
    T mergeResult(ResultOfResult<T> result);

    boolean isConcurrentNeed();

    int getThreadCount();
}
