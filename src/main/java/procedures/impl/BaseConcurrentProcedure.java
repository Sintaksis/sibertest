package procedures.impl;

import procedures.ConcurrentProcedure;
import results.Result;

public abstract class BaseConcurrentProcedure<T extends Result<?>> implements ConcurrentProcedure<T> {
    private final boolean concurrentNeed;
    private final int threadCount;

    public BaseConcurrentProcedure(boolean concurrentNeed, int threadCount) {
        this.concurrentNeed = concurrentNeed;
        this.threadCount = threadCount;
    }

    @Override
    public boolean isConcurrentNeed() {
        return concurrentNeed;
    }

    @Override
    public int getThreadCount() {
        return threadCount;
    }
}
