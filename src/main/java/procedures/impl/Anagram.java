package procedures.impl;

import procedures.Procedure;
import results.impl.ListOfStringListResult;

import java.util.*;

public class Anagram implements Procedure<ListOfStringListResult> {
    @Override
    public ListOfStringListResult handleStringList(List<String> words) {
        return findAnagram(words);
    }

    private ListOfStringListResult findAnagram(List<String> words) {
        Map<String, List<String>> allWordsMap = new HashMap<>();
        Map<String, List<String>> moreThanOneWordMap = new HashMap<>();
        for (String word : words) {
            String sortedWord = sortLetters(word);
            List<String> sortedWordsList = allWordsMap.computeIfAbsent(sortedWord, s -> new ArrayList<>());
            sortedWordsList.add(word);
            if (sortedWordsList.size() > 1) {
                moreThanOneWordMap.put(sortedWord, sortedWordsList);
            }
        }

        return new ListOfStringListResult(new ArrayList<>(moreThanOneWordMap.values()));
    }

    private String sortLetters(String word) {
        char[] wordChars = word.toCharArray();
        Arrays.sort(wordChars);
        return new String(wordChars);
    }
}
