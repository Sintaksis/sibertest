package procedures.impl;

import results.impl.IntegerResult;
import results.impl.ResultOfResult;

import java.util.List;

public class WordCounter extends BaseConcurrentProcedure<IntegerResult> {
    public WordCounter(boolean concurrentNeed, int threadCount) {
        super(concurrentNeed, threadCount);
    }

    @Override
    public IntegerResult mergeResult(ResultOfResult<IntegerResult> results) {
        int countOfWords = 0;
        for (IntegerResult result : results.getNativeResult()) {
            countOfWords += result.getNativeResult();
        }

        return new IntegerResult(countOfWords);
    }

    @Override
    public IntegerResult handleStringList(List<String> list) {
        return new IntegerResult(list.size());
    }
}
