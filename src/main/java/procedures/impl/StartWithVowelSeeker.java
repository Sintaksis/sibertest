package procedures.impl;

import results.impl.ResultOfResult;
import results.impl.StringListResult;

import java.util.ArrayList;
import java.util.List;

public class StartWithVowelSeeker extends BaseConcurrentProcedure<StringListResult> {
    private final static String VOWEL_LETTERS = "aeyuioйуеыаоэяиюё";

    public StartWithVowelSeeker(boolean concurrentNeed, int threadCount) {
        super(concurrentNeed, threadCount);
    }

    @Override
    public StringListResult handleStringList(List<String> words) {
        List<String> startWithVowelWords = new ArrayList<>();
        for (String word : words) {
            if (isVowel(word)) {
                startWithVowelWords.add(word);
            }
        }

        return new StringListResult(startWithVowelWords);
    }

    @Override
    public StringListResult mergeResult(ResultOfResult<StringListResult> results) {
        List<String> words = new ArrayList<>();
        for (StringListResult result : results.getNativeResult()) {
            String resultString = result.getResultToString();
            if (resultString.length() != 0) {
                words.add(resultString);
            }
        }

        return new StringListResult(words);
    }

    private boolean isVowel(String word) {
        if (word.length() != 0) {
            return VOWEL_LETTERS.contains(Character.toString(word.charAt(0)).toLowerCase());
        }

        return false;
    }
}
