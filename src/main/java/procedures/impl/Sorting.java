package procedures.impl;

import procedures.Procedure;
import results.impl.StringListResult;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Sorting implements Procedure<StringListResult> {
    private final static Comparator<String> wordComparator = Comparator.comparingInt(String::length);

    @Override
    public StringListResult handleStringList(List<String> words) {
        List<String> sortedWords = new ArrayList<>(words);
        sortedWords.sort(wordComparator);
        return new StringListResult(sortedWords);
    }
}
