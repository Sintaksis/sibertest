package procedures;

import results.Result;

import java.util.List;

public interface Procedure<T extends Result<?>> {
    T handleStringList(List<String> stringList);
}
