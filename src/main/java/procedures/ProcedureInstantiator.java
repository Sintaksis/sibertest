package procedures;

import results.Result;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import static com.sun.corba.se.impl.util.JDKBridge.loadClass;

public class ProcedureInstantiator<T extends Result<?>> {
    public Procedure<T> instantiateProcedure(String procedureName) throws ClassNotFoundException,
            IllegalAccessException, InstantiationException {
        @SuppressWarnings("unchecked")
        Procedure<T> procedure = (Procedure<T>) loadClass(procedureName).newInstance();
        Thread.yield();
        return procedure;
    }

    public ConcurrentProcedure<T> instantiateConcurrentProcedure(String procedureName, boolean concurrentNeed, int threadCount)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException,
                   InvocationTargetException {
        Class<?> loadedClass = loadClass(procedureName);
        Constructor<?> constructor = loadedClass.getConstructor(boolean.class, int.class);
        @SuppressWarnings("unchecked")
        ConcurrentProcedure<T> procedure = (ConcurrentProcedure<T>) constructor.newInstance(concurrentNeed, threadCount);
        return procedure;
    }
}
