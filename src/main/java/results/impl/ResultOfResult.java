package results.impl;

import results.Result;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ResultOfResult<T extends Result<?>> implements Result<List<T>> {
    private final List<T> results = Collections.synchronizedList(new ArrayList<>());

    public void addResult(T result) {
        results.add(result);
    }

    @Override
    public String getResultToString() {
        StringBuilder builder = new StringBuilder();
        for (Result<?> result : results) {
            builder.append(result.getResultToString()).append(" ");
        }

        return builder.toString();
    }

    @Override
    public List<T> getNativeResult() {
        return results;
    }
}
