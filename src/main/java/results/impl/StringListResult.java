package results.impl;

import results.Result;

import java.util.List;

public class StringListResult implements Result<List<String>> {
    private final List<String> list;

    public StringListResult(List<String> list) {
        this.list = list;
    }

    @Override
    public String getResultToString() {
        return String.join(" ", list);
    }

    @Override
    public List<String> getNativeResult() {
        return list;
    }
}
