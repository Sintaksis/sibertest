package results.impl;

import results.Result;

public class IntegerResult implements Result<Integer> {
    private final int countOfWords;

    public IntegerResult(int countOfWords) {
        this.countOfWords = countOfWords;
    }

    @Override
    public String getResultToString() {
        return String.valueOf(countOfWords);
    }

    @Override
    public Integer getNativeResult() {
        return countOfWords;
    }
}
