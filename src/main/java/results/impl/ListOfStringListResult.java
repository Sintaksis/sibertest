package results.impl;

import results.Result;

import java.util.List;

public class ListOfStringListResult implements Result<List<List<String>>> {
    private final List<List<String>> list;

    public ListOfStringListResult(List<List<String>> list) {
        this.list = list;
    }

    @Override
    public String getResultToString() {
        StringBuilder result = new StringBuilder();
        for (List<String> list : list) {
            result.append(String.join(" ", list)).append("\n");
        }

        return result.toString();
    }

    @Override
    public List<List<String>> getNativeResult() {
        return list;
    }
}
