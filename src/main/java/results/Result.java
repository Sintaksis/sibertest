package results;

public interface Result<T> {
    T getNativeResult();

    String getResultToString();
}
