package executors;

import handlers.TextHandler;
import procedures.ConcurrentProcedure;
import procedures.Procedure;
import results.Result;
import results.impl.ResultOfResult;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class Executor {
    private final static int THREAD_COUNT = 2;
    private final TextHandler textHandler = new TextHandler();
    private final ExecutorService pool = Executors.newFixedThreadPool(THREAD_COUNT);

    private void concurrentExecute(String fileName, List<String> words, ConcurrentProcedure<Result<?>> procedure) {
        AtomicInteger counter = new AtomicInteger(procedure.getThreadCount());
        ResultOfResult<Result<?>> results = new ResultOfResult<>();
        int threadCount = procedure.getThreadCount();
        List<List<String>> listsForThreads = textHandler.splitForConcurrent(words, threadCount);
        for (List<String> listForThread : listsForThreads) {
            pool.submit(new ConcurrentProcedureExecutor<>(listForThread, procedure, fileName, results, counter));
        }
    }

    public void execute(String fileName, List<String> words, List<Procedure<Result<?>>> procedures) {
        for (Procedure<Result<?>> procedure : procedures) {
            if (procedure instanceof ConcurrentProcedure) {
                ConcurrentProcedure<Result<?>> concurrentProcedure = (ConcurrentProcedure<Result<?>>) procedure;
                if (concurrentProcedure.isConcurrentNeed()) {
                    concurrentExecute(fileName, words, concurrentProcedure);
                }
            } else {
                pool.submit(new ProcedureExecutor<>(words, procedure, fileName));
            }
        }
    }

    public void shutdownExecutor() {
        pool.shutdown();
    }
}
