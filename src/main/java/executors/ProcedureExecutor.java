package executors;

import handlers.FileHandler;
import procedures.Procedure;
import results.Result;

import java.io.IOException;
import java.util.List;

public class ProcedureExecutor<T extends Result<?>> implements Runnable {
    private final FileHandler fileHandler = new FileHandler();
    private final List<String> words;
    private final Procedure<T> procedure;
    private final String fileName;

    public ProcedureExecutor(List<String> words, Procedure<T> procedure, String fileName) {
        this.words = words;
        this.procedure = procedure;
        this.fileName = fileName;
    }

    @Override
    public void run() {
        try {
            T handledWords = handleWords(words, procedure);
            fileHandler.printResult(handledWords, procedure.getClass().getTypeName(), fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private T handleWords(List<String> words, Procedure<T> procedure) {
        return procedure.handleStringList(words);
    }
}
