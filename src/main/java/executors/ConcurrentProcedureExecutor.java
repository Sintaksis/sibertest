package executors;

import handlers.FileHandler;
import procedures.ConcurrentProcedure;
import results.Result;
import results.impl.ResultOfResult;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ConcurrentProcedureExecutor<T extends Result<?>> implements Runnable {
    private final FileHandler fileHandler = new FileHandler();
    private final List<String> words;
    private final ConcurrentProcedure<T> procedure;
    private final String fileName;
    private final ResultOfResult<T> results;
    private final AtomicInteger counter;

    public ConcurrentProcedureExecutor(List<String> words, ConcurrentProcedure<T> procedure, String fileName,
                                       ResultOfResult<T> results, AtomicInteger counter) {
        this.words = words;
        this.procedure = procedure;
        this.fileName = fileName;
        this.results = results;
        this.counter = counter;
    }

    @Override
    public void run() {
        handleWords(words, procedure);
        if (counter.intValue() == 0) {
            try {
                T result = procedure.mergeResult(results);
                fileHandler.printResult(result, procedure.getClass().getTypeName(), fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void handleWords(List<String> words, ConcurrentProcedure<T> procedure) {
        T result = procedure.handleStringList(words);
        results.addResult(result);
        counter.decrementAndGet();
    }
}
