package parsers;

import procedures.Procedure;
import procedures.ProcedureInstantiator;
import results.Result;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class FileParser<T extends Result<?>> {
    private final static String DELIMITER = ":";
    private final String[] args;

    public FileParser(String[] args) {
        this.args = args;
    }

    public List<Procedure<T>> getProcedures() throws IllegalAccessException, InstantiationException,
            ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
        int startIndex = 0;
        while (true) {
            if (args[startIndex].contains(".txt")) {
                startIndex++;
                continue;
            }

            break;
        }

        String[] procedureNames = new String[args.length - startIndex];
        for (int i = 0; i < procedureNames.length; i++) {
            procedureNames[i] = args[startIndex];
            startIndex++;
        }

        List<Procedure<T>> procedures = new ArrayList<>();
        ProcedureInstantiator<T> procedureInstantiator = new ProcedureInstantiator<>();
        for (String procedureName : procedureNames) {
            String localProcedureName = procedureName;
            if (localProcedureName.contains(DELIMITER)) {
                int indexOfDelimiter = localProcedureName.indexOf(DELIMITER);
                boolean needConcurrent = true;
                String tCount = localProcedureName.substring(indexOfDelimiter + 1, localProcedureName.length());
                int threadCount = Integer.parseInt(tCount);
                localProcedureName = localProcedureName.substring(0, indexOfDelimiter);
                Procedure<T> procedure = procedureInstantiator.instantiateConcurrentProcedure(localProcedureName,
                        needConcurrent, threadCount);
                procedures.add(procedure);
            } else {
                Procedure<T> localProcedure = procedureInstantiator.instantiateProcedure(localProcedureName);
                procedures.add(localProcedure);
            }
        }

        return procedures;
    }

    public List<String> getFileNames() {
        List<String> fileNames = new ArrayList<>();
        for (String arg : args) {
            if (arg.contains(".txt")) {
                fileNames.add(arg);
                continue;
            }

            break;
        }

        return fileNames;
    }
}
