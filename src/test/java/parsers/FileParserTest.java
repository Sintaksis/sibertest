package parsers;

import org.junit.Test;
import procedures.Procedure;
import procedures.impl.Anagram;
import procedures.impl.Sorting;
import results.Result;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class FileParserTest {
    private final String[] args = new String[]{"worms.txt", "gold.txt", "procedures.impl.Anagram", "procedures.impl.Sorting"};
    private final FileParser<Result<?>> fileParser = new FileParser<>(args);

    @Test
    public void getProceduresTest() throws IllegalAccessException, ClassNotFoundException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        List<Procedure<?>> expectedProcedureList = new ArrayList<>();
        expectedProcedureList.add(new Anagram());
        expectedProcedureList.add(new Sorting());
        List<Procedure<Result<?>>> testProcedureList = fileParser.getProcedures();
        for (int i = 0; i < expectedProcedureList.size(); i++) {
            assertEquals(expectedProcedureList.get(i).getClass(), testProcedureList.get(i).getClass());
        }
    }

    @Test
    public void getFileNames() {
        List<String> expectedFileNames = new ArrayList<>();
        expectedFileNames.add("worms.txt");
        expectedFileNames.add("gold.txt");
        List<String> testStringList = fileParser.getFileNames();
        for (int i = 0; i < expectedFileNames.size(); i++) {
            assertEquals(expectedFileNames.get(i), testStringList.get(i));
        }
    }
}