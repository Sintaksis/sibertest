package handlers;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TextHandlerTest {
    private final TextHandler textHandler = new TextHandler();

    @Test
    public void splitTextForConcurrentTest() {
        int threadCount = 2;
        List<List<String>> expectedStringList = new LinkedList<>();
        List<String> firstExpectedList = new LinkedList<>();
        List<String> secondExpectedList = new LinkedList<>();
        firstExpectedList.add("hello");
        firstExpectedList.add("how");
        secondExpectedList.add("are");
        secondExpectedList.add("you");
        secondExpectedList.add("friend");
        expectedStringList.add(firstExpectedList);
        expectedStringList.add(secondExpectedList);
        List<String> testStringList = new LinkedList<>();
        testStringList.add("hello");
        testStringList.add("how");
        testStringList.add("are");
        testStringList.add("you");
        testStringList.add("friend");
        assertEquals(expectedStringList, textHandler.splitForConcurrent(testStringList, threadCount));
    }

    @Test
    public void splitStringBySpaceTest() {
        List<String> testStringList = new LinkedList<>();
        testStringList.add("hello how");
        testStringList.add("are you");
        testStringList.add("friend");
        List<String> expectedStringList = new LinkedList<>();
        expectedStringList.add("hello");
        expectedStringList.add("how");
        expectedStringList.add("are");
        expectedStringList.add("you");
        expectedStringList.add("friend");
        assertEquals(expectedStringList, textHandler.splitBySpace(testStringList));
    }
}