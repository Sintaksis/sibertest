package procedures;

import org.junit.Test;
import procedures.impl.Anagram;
import procedures.impl.WordCounter;

import java.lang.reflect.InvocationTargetException;

import static org.junit.Assert.*;

public class ProcedureInstantiatorTest {
    private final ProcedureInstantiator<?> procedureInstantiator = new ProcedureInstantiator();

    @Test
    public void instantiateProcedure() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        Procedure<?> testProcedure =
                procedureInstantiator.instantiateProcedure("procedures.impl.Anagram");
        Anagram anagram = new Anagram();
        assertEquals(anagram.getClass(), testProcedure.getClass());
    }

    @Test
    public void instantiateConcurrentProcedure() throws ClassNotFoundException, NoSuchMethodException,
            InvocationTargetException, InstantiationException, IllegalAccessException {
        ConcurrentProcedure<?> testProcedure =  procedureInstantiator
                        .instantiateConcurrentProcedure("procedures.impl.WordCounter", true, 3);
        WordCounter wordCounter = new WordCounter(true, 3);
        assertEquals(wordCounter.getThreadCount(), testProcedure.getThreadCount());
        assertEquals(wordCounter.isConcurrentNeed(), testProcedure.isConcurrentNeed());
    }
}