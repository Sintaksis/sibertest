package procedures.impl;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AnagramTest {
    private final Anagram anagram = new Anagram();

    @Test
    public final void testHandleString() {
        String testString = "jump pumj\nolleh hello\n";
        List<String> testStringList = new ArrayList<>();
        testStringList.add("olleh");
        testStringList.add("go");
        testStringList.add("hello");
        testStringList.add("jump");
        testStringList.add("pumj");
        testStringList.add("yes");
        assertEquals(testString, anagram.handleStringList(testStringList).getResultToString());
    }
}