package procedures.impl;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SortingTest {
    private final Sorting sorting = new Sorting();

    @Test
    public final void testHandleString() {
        String resultString = "go yes jump hello";
        List<String> testStringList = new LinkedList<>();
        testStringList.add("jump");
        testStringList.add("go");
        testStringList.add("hello");
        testStringList.add("yes");
        assertEquals(resultString, sorting.handleStringList(testStringList).getResultToString());
    }
}