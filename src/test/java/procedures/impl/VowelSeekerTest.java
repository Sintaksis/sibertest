package procedures.impl;

import org.junit.Test;
import results.impl.ResultOfResult;
import results.impl.StringListResult;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class VowelSeekerTest {
    private final StartWithVowelSeeker vowelSeeker = new StartWithVowelSeeker(false, 0);

    @Test
    public final void testHandleString() {
        List<String> testStringList = new LinkedList<>();
        testStringList.add("yes");
        testStringList.add("no");
        testStringList.add("please");
        testStringList.add("go");
        testStringList.add("oops");
        testStringList.add("executors");
        testStringList.add("ice");
        String resultString = "yes oops executors ice";
        assertEquals(resultString, vowelSeeker.handleStringList(testStringList).getResultToString());
    }

    @Test
    public void uniteResult() {
        ResultOfResult<StringListResult> results = new ResultOfResult<>();
        List<String> testStringList = new LinkedList<>();
        List<String> rightStringList = new LinkedList<>();
        testStringList.add("one");
        testStringList.add("two");
        testStringList.add("three");
        results.addResult(new StringListResult(testStringList));
        rightStringList.add("one two three");
        assertEquals(new StringListResult(rightStringList).getResultToString(), vowelSeeker.mergeResult(results).getResultToString());
    }
}