package procedures.impl;

import org.junit.Test;
import results.impl.IntegerResult;
import results.impl.ResultOfResult;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class WordCounterTest {
    private final WordCounter wordCounter = new WordCounter(false, 0);

    @Test
    public void handleStringList() {
        List<String> testStringList = new LinkedList<>();
        testStringList.add("oh");
        testStringList.add("no");
        testStringList.add("great");
        testStringList.add("loop");
        testStringList.add("while");
        testStringList.add("executors");
        testStringList.add("thread");
        assertEquals(String.valueOf(7), wordCounter.handleStringList(testStringList).getResultToString());
    }

    @Test
    public void uniteResult() {
        ResultOfResult<IntegerResult> results = new ResultOfResult<>();
        List<String> testStringList = new LinkedList<>();
        testStringList.add("one");
        testStringList.add("two");
        testStringList.add("three");
        results.addResult(new IntegerResult(testStringList.size()));
        assertEquals(new IntegerResult(3).getResultToString(), wordCounter.mergeResult(results).getResultToString());
    }
}